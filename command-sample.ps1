$network_dim = 8
$learning_rate = 1e-3
$max_train_epochs = 2

& accelerate launch --num_cpu_threads_per_process 4 train_network.py `
--pretrained_model_name_or_path=$model_path `
--dataset_config=$dataset_path `
--output_dir=$output_dir `
--output_name="$dataset_name-$network_dim-$learning_rate-$max_train_epochs" `
--train_batch_size=1 `
--network_module=networks.lora `
--network_dim=$network_dim `
--network_alpha=1 `
--network_args `
--lr_scheduler="cosine_with_restarts" `
--lr_scheduler_num_cycles=1 `
--optimizer_type="AdamW8bit" `
--optimizer_args `
--learning_rate=$learning_rate `
--lr_warmup_steps=0 `
--max_train_epochs=$max_train_epochs `
--save_every_n_epochs=1 `
--save_model_as=safetensors `
--prior_loss_weight=0.1 `
--mixed_precision="fp16" `
--save_precision="fp16" `
--max_data_loader_n_workers=4 `
--clip_skip=2 `
--max_token_length=150 `
--min_snr_gamma=5 `
--scale_weight_norms=1.0 `
--xformers `
--weighted_captions `
--gradient_checkpointing `
--persistent_data_loader_workers `
--cache_latents