# sd-scripts-starter
 
It is a utility script that quickly starts learning your Lora.
 
## Features
 
You can start learning with your custom dataset.toml and command.ps1
 
## Requirement
 
* PowerShell 5.1 or above
* [sd-scripts](https://github.com/kohya-ss/sd-scripts)

## Usage
 
1. Make your env.ini from env.default.ini in the config folder.  
2. Make your dataset.toml from dataset-sample.toml.  
3. Custom command-sample.ps1 if you need.
4. Run start.ps1. (You would set the ExecutionPolicy of PowerShell.)  
5. Follow the script instructions.  
Input some paths (or hit Enter to use the default path that is in env.ini).  
You can drag and drop to input the path.  
 
## Author
 
* Koji Hasegawa
* Hasegawa Mech Ggarage
* on.a.snowy.night@gmail.com
 
## License
 
sd-scripts-starter is under [MIT license](https://opensource.org/license/mit/).
