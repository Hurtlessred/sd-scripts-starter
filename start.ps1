$ini_path = '.\config\env.ini'

if (-not (Test-Path -Path $ini_path -PathType Leaf))
{
    Write-Output "ini file not found : $ini_path"
    Write-Output "Please make your $ini_path file from env.default.ini"
    Pause
    exit
}

$ini = Get-Content $ini_path | ConvertFrom-StringData

# sd_scripts
$sd_scripts_dir = $ini.sd_scripts_dir
$sd_scripts_dir = $sd_scripts_dir.Replace('"','')

if (-not (Test-Path -Path $sd_scripts_dir))
{
    Write-Output "sd-script dir not found : $sd_scripts_dir"
    Pause
    exit
}

Write-Output "You can Drag & Drop to input file path"

# Dataset
$dataset_default = $ini.dataset_default
$dataset_path = Read-Host "Input dataset file (toml) path [or hit Enter to use $dataset_default ]"

if ($dataset_path -eq "")
{
    $dataset_path = $dataset_default
}

$dataset_path = $dataset_path.Replace('"','')

if (-not (Test-Path -Path $dataset_path -PathType Leaf))
{
    Write-Output "Dataset file not found : $cdataset_path"
    Pause
    exit
}

$dataset_path = Convert-Path $dataset_path
$dataset_name = [System.IO.Path]::GetFileNameWithoutExtension($dataset_path)

# Command
$command_default = $ini.command_default
$command_path = Read-Host "Input command file (ps1) path [or hit Enter to use $command_default ]"

if ($command_path -eq "")
{
    $command_path = $command_default
}

$command_path = $command_path.Replace('"','')

if (-not (Test-Path -Path $command_path -PathType Leaf))
{
    Write-Output "Command file not found : $command_path"
    Pause
    exit
}

$command_path = Convert-Path $command_path

# Model
$model_default = $ini.model_default
$model_path = Read-Host "Input model file (safetensor) path [or hit Enter to use $model_default ]"

if ($model_path -eq "")
{
    $model_path = $model_default
}

$model_path = $model_path.Replace('"','')

if (-not (Test-Path -Path $model_path -PathType Leaf))
{
    Write-Output "Model file not found : $model_path"
    Pause
    exit
}

# Output Dir
$output_dir_default = $ini.output_dir_default
$output_dir = Read-Host "Input output dir path [or hit Enter to use $output_dir_default ]"

if ($output_dir -eq "")
{
    $output_dir = $output_dir_default
}

$output_dir = $output_dir.Replace('"','')

if (-not (Test-Path -Path $output_dir))
{
    Write-Output "Dir not found : $output_dir"
    Pause
    exit
}

# Confirmation
Write-Output ""

Write-Output "Dataset : $dataset_path" 
Get-Content $dataset_path | Write-Output
Write-Output ""

Write-Output "Command : $command_path"
Write-Output ""

Write-Output "Model : $model_path"
Write-Output ""

Write-Output "Output Dir : $output_dir"
Write-Output ""

Write-Output "Please Enter to start lerning"
Pause
Write-Output "Starting lerning..."

# Lerning
Set-Location $sd_scripts_dir
venv\Scripts\activate.ps1

. $command_path

Pause
exit